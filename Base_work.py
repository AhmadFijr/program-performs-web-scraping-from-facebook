# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 19:38:54 2021

@author: Ahmad Fijr
"""


import sqlite3,os
from sqlite3 import OperationalError


""" This function is to create the database and enter data in it """
def DataBase_work(nameIt, General_list, Contact_Info_list,about, Additional_Information, Date_of_First_Post,Date_of_last_Post,Likes_of_last_Post,Peoples_names_Likes_of_last_Post):
    
    # If the database already exists, delete it
    if nameIt in os.listdir(os.getcwd()):
        os.remove(nameIt)
    
    # Connect and create the database
    connect = sqlite3.connect(nameIt)
    cur = connect.cursor()
    
    # Create table of info and Last post
    try:
        cur.execute('''CREATE TABLE info
              (General text, "CONTACT INFO" text, About text, "Additional Information" text, "Date of first post" text)''')

        cur.execute('''CREATE TABLE "Last post"
               ("Date of last Post" text, "Likes" real, "Peoples names" text)''')
    except OperationalError as e:
        print("sqlite3 Error:",e)

    # Entering the information into the database
    text1 = "INSERT INTO info VALUES ('{}','{}','{}',\"{}\",'{}')".format(General_list[0], Contact_Info_list[0], about, Additional_Information, Date_of_First_Post)
    text2 = "INSERT INTO info VALUES ('{}','{}','','','')".format(General_list[1], Contact_Info_list[1])
    text3 = "INSERT INTO info VALUES ('{}','','','','')".format(General_list[2])
    cur.execute(text1)
    cur.execute(text2)
    cur.execute(text3)

    text1 = "INSERT INTO \"Last post\" VALUES ('{}',{},'{}')".format(Date_of_last_Post,Likes_of_last_Post,Peoples_names_Likes_of_last_Post[0])
    cur.execute(text1)

    for i in range(1,len(Peoples_names_Likes_of_last_Post)):
        text = "INSERT INTO \"Last post\" VALUES ('','',\"{}\")".format(Peoples_names_Likes_of_last_Post[i])
        cur.execute(text)

    #Save and close the database
    connect.commit()
    connect.close()