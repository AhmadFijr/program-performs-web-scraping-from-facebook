# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 16:54:59 2021

@author: Ahmad Fijr
"""

from webdriver_manager.firefox import GeckoDriverManager
from selenium import webdriver
from selenium.common.exceptions import WebDriverException,NoSuchElementException, TimeoutException, ElementClickInterceptedException,StaleElementReferenceException

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from time import sleep
import shutil
import os

from datetime import datetime, timedelta
import urllib

""""Auxiliary functions"""
###############################################################################

# Automatically download the driver that matches your FireFox browser and move it to the current path
def __Download_Driver():
    shutil.copy(GeckoDriverManager().install(),os.getcwd())

"""Check if the driver is present in the current directory"""
#If it is present then do nothing, and if it does not exist then Download it
def Check_Download():
    if "geckodriver.exe" not in os.listdir():
        __Download_Driver()
        return "geckodriver.exe"
    else:
        return "geckodriver.exe"

"""The sleep function works but is timed and is specially designed to complete
 the work after the device is turned on when it is closed in hibernation mode"""
def __time_mange(time_AfterNow,cheak_Time_Evrey):
    now = datetime.now()
    after_time = now + timedelta(0,time_AfterNow)
    while True:
        now = datetime.now()
        if now >= after_time:
            print("now >= after_time")
            break
        else:
            sleep(cheak_Time_Evrey)
            
# To download an image from a link
def Download_pic(src,path):
    req = urllib.request.Request(src, headers={'User-Agent': 'Mozilla/5.0'})
    with open(path, "wb") as f:
        with urllib.request.urlopen(req) as r:
            f.write(r.read())

"""Basic functions of operation"""
###############################################################################
            
def run_browser():
    global driver,action
    while True:
        
        try:
            # Create an automation session with FireFox
            driver = webdriver.Firefox(executable_path=Check_Download(), timeout=60000, keep_alive=True)
            action = webdriver.ActionChains(driver)
            break
        
        except TimeoutException as e:
            # If you are unable to create the session, close the Driver, close all FireFox extensions, wait two minutes, then try again.
            print("TimeoutException:",e)
            os.system('taskkill /IM firefox.exe /F')
            os.system('taskkill /IM geckodriver.exe /F')
            print("sleep for two min and then try to run browser again")
            __time_mange(60 * 2, 30)
            


# To open a site           
def web_GoTo(url):
    try:
        driver.get(url)
    except WebDriverException as e:
        #If going to the site fails, wait two minutes, then try again
        print(e)
        print("sleep for two min and then try to go website again")
        __time_mange(60 * 2,30)

def __LoginSaveNotNow():
    for i in driver.find_elements(By.TAG_NAME,"a"):
        if i.get_attribute("href") == 'https://mbasic.facebook.com/login/save-device/cancel/?flow=interstitial_nux&nux_source=regular_login':
            i.click()
            break
        
def facebook_login(email, password):
        web_GoTo('https://mbasic.facebook.com/')
        email_name = 'email'
        password_name = 'pass'
        login_button_name = 'login'
        email_element = driver.find_element_by_name(email_name)
        password_element = driver.find_element_by_name(password_name)
        login_button_element = driver.find_element_by_name(login_button_name)
        email_element.send_keys(email)
        password_element.send_keys(password)
        login_button_element.click()
        sleep(5)
        __LoginSaveNotNow()
        
def close_browser():
    try:
        driver.quit()
    except Exception as e:
        print(e)
        os.system('taskkill /IM firefox.exe /F')
    finally:
        os.system('taskkill /IM geckodriver.exe /F')
    

"""Crawling policy functions """
###############################################################################

# Get all Info section data
def get_info(link):
    web_GoTo(link+'about')
    
    driver.implicitly_wait(30)
    all_info = driver.find_element(By.CLASS_NAME,'db0gmjza')
    driver.implicitly_wait(3)
    
    seeMore = all_info.find_element(By.CSS_SELECTOR,'div.oo9gr5id')
    seeMore.click()
    
    tmp = all_info.find_elements(By.CLASS_NAME,'je60u5p8')
    for i in tmp:
        text = i.text
        
        if "GENERAL" in text:        
            GENERAL_text = text
            
        elif 'ADDITIONAL CONTACT INFO' in text:
            CONTACT_INFO_text = text
        
        elif 'MORE INFO' in text:
            MORE_INFO_text = text
                
    return GENERAL_text,CONTACT_INFO_text,MORE_INFO_text       

# Get Profile picture & main header picture
def get_pictures(link):
    web_GoTo(link+'about')
    driver.implicitly_wait(30)
    header = driver.find_element(By.CSS_SELECTOR,".nq2o82sz")
    src = header.get_attribute("src")
    Download_pic(src,'header.png')
    driver.implicitly_wait(3)
    
    Profile = driver.find_element(By.CSS_SELECTOR,"a.q9uorilb:nth-child(1) > div:nth-child(1) > svg:nth-child(1) > g:nth-child(2) > image:nth-child(1)")
    Profile.click()
    
    driver.implicitly_wait(30)
    picture = driver.find_element(By.CSS_SELECTOR,".ji94ytn4")
    picture.screenshot('Profile.png')
    driver.implicitly_wait(3)

# Get Date of first post
def get_datefirst_post(link):
    web_GoTo(link.replace("www.","mbasic."))
    dates = driver.find_elements(By.CLASS_NAME,"j")
    last_date = dates[len(dates)-1]
    last = last_date.find_element(By.TAG_NAME,'a')
    last.click()
    
    posts = driver.find_elements(By.TAG_NAME,"article")
    first_post = posts[len(posts)-1]
    date = first_post.find_element(By.CLASS_NAME,"da").text
    return date

# Get Number of likes on last post, date of last post, name of people who liked the last post
def get_last_post(link):
    people_likes = []
    
    web_GoTo(link.replace("www.","mbasic."))

    Posts = driver.find_elements(By.TAG_NAME,"article")
    last_post = Posts[0]
    for i in last_post.find_elements(By.TAG_NAME,"a"):
        if i.text == "Full Story":
            link = i.get_attribute("href")
            Link_last_post = link
            break
        
    web_GoTo(Link_last_post.replace("mbasic.","m."))
    Post = driver.find_element(By.CLASS_NAME,"story_body_container")
    tmp = Post.find_element(By.TAG_NAME,"abbr")
    date = tmp.text
    
    
    liked = driver.find_element(By.CSS_SELECTOR,"._45m8")
    likes = liked.text
    liked.click()
    
    while True:
        driver.implicitly_wait(3)
        try:
            See_More = driver.find_element(By.ID,"reaction_profile_pager")
            if See_More.text == 'See More…':
                See_More.click()
        except NoSuchElementException:
            break
        except StaleElementReferenceException:
            break
        except ElementClickInterceptedException:
            pass
        
    items = driver.find_elements(By.CLASS_NAME,"item")
    for i in items:
        people_likes.append(i.text.replace("\nAdd Friend","").replace("\nFollow",""))
    
    return date,likes,people_likes