# -*- coding: utf-8 -*-

email = 'xxxxxxx@gmail.com'
password = 'xxxxxxxx'
link = 'https://www.facebook.com/vietfunnyvideo'


if not link.endswith('/'):
    link += '/'
    
from FaceCC import run_browser, facebook_login, get_info, get_datefirst_post, get_last_post, close_browser,get_pictures
from Base_work import DataBase_work

# Get data from Facebook & return data
run_browser()

facebook_login(email,password)

get_pictures(link)

GENERAL_text, CONTACT_INFO_text, MORE_INFO_text = get_info(link)

date_of_firstpost = get_datefirst_post(link)

date, likes, people_likes = get_last_post(link)

close_browser()

# Data processing
General_list = GENERAL_text.split('\n')[1:]
Contact_Info_list = CONTACT_INFO_text.split('\n')[1:]
about = MORE_INFO_text.split('Additional Information')[0].replace("MORE INFO","").replace("About","").replace("\n","")
Additional_Information = MORE_INFO_text.split('Additional Information')[1].replace("\"","").replace("\n","").replace(" See Less","")

Date_of_First_Post = date_of_firstpost.replace(" · Custom","")

Date_of_last_Post = date
Likes_of_last_Post = likes
Peoples_names_Likes_of_last_Post = people_likes

# Data printing
print("Profile picture & main header picture in Current path"+'\n')
print(GENERAL_text+'\n')
print(CONTACT_INFO_text+'\n')
print('about:',about+'\n')
print('Additional_Information: ',Additional_Information+'\n')
print('Date of First Post:',Date_of_First_Post)
print("Date of last Post:",Date_of_last_Post)
print("Likes of last Post:",Likes_of_last_Post+'\n')
print("Peoples names Likes of last Post:"+'\n',Peoples_names_Likes_of_last_Post)

# Save information in the database
DataBase_work("database.db", General_list, Contact_Info_list,about, Additional_Information, Date_of_First_Post,Date_of_last_Post,Likes_of_last_Post,Peoples_names_Likes_of_last_Post)


 
